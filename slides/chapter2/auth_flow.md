\begin{figure}[c]
\tikzset{
  % add this style to all tikzpicture environments
  every picture/.append style={
    % enable scaling of nodes
    transform shape,
    % set scale factor
    scale=0.45
  }
}
\begin{sequencediagram}
    \renewcommand\unitfactor{0.8}
    \newinst{client}{Browser}
    \newinst[10]{frontend}{Webserver}
    \newinst[10]{idp}{Identity Provider (Keycloak)}
    
    \begin{call}{client}{GET https://mail.nxt}{frontend}{
        \shortstack[l]{
        302 https://auth.nxt/realms/nxt/protocol/openid-connect/auth\\
        ?\textbf{state}=... (CSRF protection)\\
        \&\textbf{nonce}=... (serverside replay protection)\\
        \&\textbf{scope}=openid email profile\\
        \&\textbf{response\_type}=code\\
        \&\textbf{redirect\_uri}=https://mail.nxt/callback\\
        \&\textbf{client\_id}=mail}}
        \postlevel
        \postlevel
        \begin{call}{frontend}{GET https://auth.nxt/.well-known/openid-configuration}{idp}{\{"authorization\_endpoint": "...", ...\}}
        \end{call}
    \end{call}
    
    
    \begin{call}{client}{GET https://auth.nxt/realms/nxt/protocol/openid-connect/auth?...}{idp}{Login Form}
             
    \end{call}
    
    \begin{call}{client}{Performs login}{idp}{
         \shortstack[l]{
         302 https://mail.nxt/callback\\
         ?\textbf{state}=...\\
         \&\textbf{code}=...}}
         \postlevel
    \end{call}
    
     \postlevel
    
    \begin{call}{client}{GET https://mail.nxt/callback?}{frontend}{302 https://mail.nxt}
        \begin{call}{frontend}{
            \shortstack[l]{
            POST https://auth.nxt/realms/nxt/protocol/openid-connect/token\\
            \textbf{client\_id}=mail\\
            \textbf{grant\_type}=authorization\_code\\
            \textbf{code}=...\\
            \textbf{state}=...}
        }{idp}{\{"id\_token": "...", "access\_token"="..." \}}
                                             
        \end{call}
        
        \begin{call}{frontend}{Verify token}{frontend}{}
                                                  
        \end{call}
            
    \end{call}
   
\end{sequencediagram}
\end{figure}