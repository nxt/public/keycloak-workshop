# OAuth2 / OpenID Connect

## OpenID Connect vs OAuth2

OpenID Connect (OIDC) is an authentication layer on top of OAuth 2.0, an authorization framework.

## Scenario

A user wants to open a web application which uses google as its identity provider.

1. User opens the web application
2. Web application redirects the user to google with a request to access the profile information
3. User logs on to google and allows mail application to access profile data
4. Google forwards the user with his profile data to the mail application

## Scope

Scopes are space-separated lists of identifiers used to specify which claims are requested. 

| scope   | required | purpose                                                                          |
|---------|----------| ---------------------------------------------------------------------------------|
| openid  | X        | indicates that the application intends to use OIDC to verify the user's identity |
| profile | O        | default profile claims (name, family_name, given_name, ...)                      | 
| email   | O        | email and email_verified claim                                                   |
| address | O        | address claim                                                                    |
| phone   | O        | phone_number and phone_number_verified claims                                    |

## Claims

Claims are name/value pairs that contain information about the user and meta-information about the OIDC service.

\tiny

```json
{
  "jti": "ec6f7d68-6d20-4d58-b25b-c99a26eb001e",
  "exp": 1565251107,
  "nbf": 0,
  "iat": 1565250807,
  "iss": "http://localhost:8080/auth/realms/demo",
  "aud": "ng-app",
  "sub": "30d051b7-b632-4ec9-b4e5-2a89f6c05c2f",
  "typ": "ID",
  "azp": "ng-app",
  "nonce": "N4hAoxmoYYBxCW3OlUZX2ceA0iix2LZgucpAVhoFevwjq",
  "auth_time": 1565250806,
  "session_state": "9c03cf83-5125-477f-9bff-0ac86102667b",
  "acr": "1",
  "email_verified": false,
  "name": "Bruce Wilson",
  "preferred_username": "bwilson",
  "given_name": "Bruce",
  "family_name": "Wilson",
  "email": "bwilson@keycloak.org"
}
```

\normalsize

## JSON Web Token 

\textcolor{red}{eyJhbGci}.\textcolor{blue}{eyJzdWIiOiIxMjM}.\textcolor{violet}{eyJzdWIiOiIxMjM}
=>
\textcolor{red}{Header}.\textcolor{blue}{Payload}.\textcolor{violet}{Signature}

**Header**

\scriptsize
```json
{
  "alg": "RS256",
  "typ": "JWT"
}
```
\normalsize

**Payload**

\scriptsize
```json
{
   "jti": "ec6f7d68-6d20-4d58-b25b-c99a26eb001e",
   "iss": "http://localhost:8080/auth/realms/demo",
   "name": "Bruce Wilson"
}
```
\normalsize

**Signature**

\scriptsize
```javascript
RSASHA256(
  base64UrlEncode(header) + "." + base64UrlEncode(payload),
  PUBLIC_KEY)
```
\normalsize

## OIDC Authorisation Code Flow 

!include auth_flow.md

## OIDC Implicit Flow 

!include implicit_flow.md

## OIDC Authorisation Code Flow + Proof Key for Code Exchange

!include auth_flow_pkce.md