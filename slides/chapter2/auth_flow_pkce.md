\begin{figure}[c]
\tikzset{
  % add this style to all tikzpicture environments
  every picture/.append style={
    % enable scaling of nodes
    transform shape,
    % set scale factor
    scale=0.42
  }
}
\begin{sequencediagram}
    \renewcommand\unitfactor{0.8}
    \newinst{client}{Browser}
    \newinst[10]{frontend}{Webserver}
    \newinst[10]{idp}{Identity Provider (Keycloak)}
    
    \begin{call}{client}{GET https://mail.nxt}{frontend}{
        \shortstack[l]{
        302 https://auth.nxt/realms/nxt/protocol/openid-connect/auth\\
        ?state=... (CSRF protection)\\
        \&nonce=... (serverside replay protection)\\
        \&scope=openid email profile\\
        \&\textbf{response\_type}=code\\
        \&redirect\_uri=https://mail.nxt/callback\\
        \&\textbf{code\_challenge}=...\\
        \&\textbf{code\_challenge\_method}=S256\\
        \&client\_id=mail}}
        \postlevel
        \begin{call}{frontend}{GET https://auth.nxt/.well-known/openid-configuration}{idp}{\{"authorization\_endpoint": "...", ...\}}
        \end{call}
        
        \begin{call}{frontend}{Generate Code Verifier (i.e. "Hello") and Code Challenge (sha256("Hello"))}{frontend}{}
        \end{call}
    \end{call}
    
    
    \begin{call}{client}{GET https://auth.nxt/realms/nxt/protocol/openid-connect/auth?...}{idp}{Login Form}
             
    \end{call}
    
    \begin{call}{client}{Performs login}{idp}{
         \shortstack[l]{
         302 https://mail.nxt/callback\\
         ?state=...\\
         \&code=...}}
         \postlevel
    \end{call}
    
    \postlevel
    \postlevel
    
    \begin{call}{client}{GET https://mail.nxt/callback?}{frontend}{302 https://mail.nxt}
        \begin{call}{frontend}{
            \shortstack[l]{
            POST https://auth.nxt/realms/nxt/protocol/openid-connect/token\\
            client\_id=mail\\
            grant\_type=authorization\_code\\
            code=...\\
            \textbf{code\_verifier}=...\\
            state=...}
        }{idp}{\{"id\_token": "...", "access\_token"="..." \}}
                                             
        \end{call}
        
        \begin{call}{frontend}{Verify token}{frontend}{}
                                                  
        \end{call}
            
    \end{call}
   
\end{sequencediagram}
\end{figure}