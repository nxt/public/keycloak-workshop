\begin{figure}[c]
\tikzset{
  % add this style to all tikzpicture environments
  every picture/.append style={
    % enable scaling of nodes
    transform shape,
    % set scale factor
    scale=0.45
  }
}
\begin{sequencediagram}
    \renewcommand\unitfactor{0.8}
    \newinst{client}{Browser}
    \newinst[10]{frontend}{Webserver}
    \newinst[10]{idp}{Identity Provider (Keycloak)}
    
    \begin{call}{client}{GET https://mail.nxt}{frontend}{
        \shortstack[l]{
        302 https://auth.nxt/realms/nxt/protocol/openid-connect/auth\\
        ?state=... (CSRF protection)\\
        \&nonce=... (serverside replay protection)\\
        \&scope=openid email profile\\
        \&\textbf{response\_type=id\_token token}\\
        \&redirect\_uri=https://mail.nxt/callback\\
        \&client\_id=mail}}
        \postlevel
        \postlevel
        \begin{call}{frontend}{GET https://auth.nxt/.well-known/openid-configuration}{idp}{\{"authorization\_endpoint": "...", ...\}}
        \end{call}
    \end{call}
    
    
    \begin{call}{client}{GET https://auth.nxt/realms/nxt/protocol/openid-connect/auth?...}{idp}{Login Form}
             
    \end{call}
    
     \postlevel
    
    \begin{call}{client}{Performs login}{idp}{
         \shortstack[l]{
         302 https://mail.nxt/callback\\
         ?state=...\\
         ?\textbf{session\_state}=...\\
         ?\textbf{id\_token}=...\\
         ?\textbf{access\_token}=...\\
         ?\textbf{token\_type}=bearer\\
         \&\textbf{expires\_in}=900}}
         \postlevel
         \postlevel
         \postlevel
         \postlevel
    \end{call}
    
\end{sequencediagram}
\end{figure}

