# Overview

## Keycloak - Open Source Identity and Access Management

### Developer

RedHat

### License

Apache License version 2.0

### GitHub

https://github.com/keycloak/keycloak

## Requirements for this Workshop

- IDE (IntelliJ, VSCode)
- Docker
- Browser (Chrome, Firefox, Safari)
- (Apache Directory Studio)