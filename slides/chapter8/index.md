# Authentication Service Provider Interface (SPI)

## Terms  {.allowframebreaks}

### Authentication Flow

A flow is a container for all authentications that must happen during login or registration. 
If you go to the admin console authentication page, you can view all the defined flows in the system and what authenticators they are made up of. 
Flows can contain other flows. 
You can also bind a new different flow for browser login, direct grant access, and registration.

### Authenticator
An authenticator is a pluggable component that holds the logic for performing the authentication or action within a flow. 
It is usually a singleton.

### Execution
An execution is an object that binds the authenticator to the flow and the authenticator to the configuration of the authenticator. 
Flows contain execution entries.

### Execution Requirement
Each execution defines how an authenticator behaves in a flow. 
The requirement defines whether the authenticator is enabled, disabled, optional, required, or an alternative. 
An alternative requirement means that the authenticator is optional unless no other alternative authenticator is successful in the flow. 
For example, cookie authentication, kerberos, and the set of all login forms are all alternative. If one of those is successful, none of the others are executed.

### Authenticator Config
This object defines the configuration for the Authenticator for a specific execution within an authentication flow. 
Each execution can have a different config.

### Required Action
After authentication completes, the user might have one or more one-time actions he must complete before he is allowed to log in. 
The user might be required to set up an OTP token generator or reset an expired password or even accept a Terms and Conditions document.

## New browser flow

```
Cookie - ALTERNATIVE
Kerberos - ALTERNATIVE
Forms Subflow - ALTERNATIVE
           Username/Password Form - REQUIRED
           Math Authenticator - REQUIRED
```

## Packaging and Deployment

* Single jar file located in the `/opt/jboss/keycloak/standalone/deployments/` directory.
* File with the name `org.keycloak.authentication.AuthenticatorFactory` located in the `META-INF/services/` directory which contains the fully qualified classname of the authenticator factory.

## Implementing the Math Authenticator Factory I

\tiny

```java
public class MathAuthenticatorFactory implements AuthenticatorFactory {

    private static final String PROVIDER_ID = "math-authenticator";
    private static final MathAuthenticator SINGLETON = new MathAuthenticator();

    public String getId() {
        return PROVIDER_ID;
    }

    public Authenticator create(KeycloakSession keycloakSession) {
        return SINGLETON;
    }

    /**
     * While there are four different requirement types: ALTERNATIVE, REQUIRED, OPTIONAL, DISABLED,
     * AuthenticatorFactory implementations can limit which requirement options are shown in the admin console when defining a flow.
     */
    public AuthenticationExecutionModel.Requirement[] getRequirementChoices() {
        return new AuthenticationExecutionModel.Requirement[]{
                AuthenticationExecutionModel.Requirement.REQUIRED,
                AuthenticationExecutionModel.Requirement.DISABLED
        };
    }

    /**
     * Text that will be shown in the admin console when listing the Authenticator.
     */
    public String getDisplayType() {
        return "Math Authenticator";
    }
```

## Implementing the Math Authenticator Factory II

\tiny

```java
    /**
     * Tooltip text that will be shown when you are picking the Authenticator you want to bind to an execution.
     */
    public String getHelpText() {
        return "Math Authenticator";
    }

    /**
     * Category the Authenticator belongs to.
     */
    public String getReferenceCategory() {
        return null;
    }

    /**
     * The isUserSetupAllowed() is a flag that tells the flow manager whether or not Authenticator.setRequiredActions() method will be called.
     * If an Authenticator is not configured for a user, the flow manager checks isUserSetupAllowed().
     * If it is false, then the flow aborts with an error.
     * If it returns true, then the flow manager will invoke Authenticator.setRequiredActions().
     */
    public boolean isUserSetupAllowed() {
        return false;
    }

    /**
     * Flag which specifies to the admin console on whether the Authenticator can be configured within a flow
     */
    public boolean isConfigurable() {
        return true;
    }
```

## Implementing the Math Authenticator Factory III

\tiny

```java
    /**
     * Configuration option which is shown in the admin console.
     */
    public List<ProviderConfigProperty> getConfigProperties() {
        List<ProviderConfigProperty> configProperties = new ArrayList<>();
        ProviderConfigProperty property = new ProviderConfigProperty();
        property.setName("equation");
        property.setLabel("Equation");
        property.setType(ProviderConfigProperty.STRING_TYPE);
        property.setHelpText("Equation");
        configProperties.add(property);

        property = new ProviderConfigProperty();
        property.setName("solution");
        property.setLabel("Solution");
        property.setType(ProviderConfigProperty.STRING_TYPE);
        property.setHelpText("Solution");
        configProperties.add(property);
        return configProperties;
    }
```

## Implementing the Math Authenticator Factory IV

\tiny

```java
    /**
     * Only called once when the factory is first created.
     */
    public void init(Config.Scope scope) {
    }

    /**
     * Called after all provider factories have been initialized
     */
    public void postInit(KeycloakSessionFactory keycloakSessionFactory) {

    }

    /**
     * This is called when the server shuts down.
     */
    public void close() {
    }
}
```

\normalsize

## Implementing the Math Authenticator I

\tiny

```java
public class MathAuthenticator implements Authenticator {

    /**
     * Does this authenticator require that the user has already been identified? 
     */
    public boolean requiresUser() {
        return false;
    }

    /**
     * Initial call for the authenticator.  This method should check the current HTTP request to determine if the request
     * satifies the Authenticator's requirements.  If it doesn't, it should send back a challenge response by calling
     * the AuthenticationFlowContext.challenge(Response).
     */
    public void authenticate(AuthenticationFlowContext context) {
        String equation = context.getAuthenticatorConfig().getConfig().get("equation");
        context.challenge(context.form().setAttribute("equation", equation).createForm("math.ftl"));
    }
```

## Implementing the Math Authenticator II

\tiny

```java
    /**
     * Called from a form action invocation.
     */
    public void action(AuthenticationFlowContext context) {
        boolean validated = context.getHttpRequest().getDecodedFormParameters().getFirst("answer")
                .equals(context.getAuthenticatorConfig().getConfig().get("solution"));
        if (!validated) {
            context.failureChallenge(AuthenticationFlowError.INVALID_CREDENTIALS, context.form()
                    .setAttribute("equation", context.getAuthenticatorConfig().getConfig().get("equation"))
                    .setError("badResult")
                    .createForm("math.ftl"));
            return;
        }
        context.success();
    }
    /**
     * This method is responsible for determining if the user is configured for this particular authenticator.
     */
    public boolean configuredFor(KeycloakSession session, RealmModel realm, UserModel user) {
        return false;
    }
    /**
     * Set actions to configure authenticator.
     */
    public void setRequiredActions(KeycloakSession session, RealmModel realm, UserModel user) {
    }
    /**
     * This is called when the server shuts down.
     */
    public void close() {
    }
}
```

## FreeMarker Template


\tiny

```html
<#import "template.ftl" as layout>
<@layout.registrationLayout; section>
    <#if section = "header">
        ${msg("doLogIn")}
    <#elseif section = "form">
        <form id="kc-totp-login-form" class="${properties.kcFormClass!}" action="${url.loginAction}" method="post">
            <div class="${properties.kcFormGroupClass!}">
                <div class="${properties.kcLabelWrapperClass!}">
                    <label for="answer" class="${properties.kcLabelClass!}">${equation} = ?</label>
                </div>

                <div class="${properties.kcInputWrapperClass!}">
                    <input id="answer" name="answer" type="text" class="${properties.kcInputClass!}" />
                </div>
            </div>

            <div class="${properties.kcFormGroupClass!}">
                <div id="kc-form-buttons" class="${properties.kcFormButtonsClass!}">
                    <div class="${properties.kcFormButtonsWrapperClass!}">
                        <input class="${properties.kcButtonClass!}" name="login" 
                               id="kc-login" type="submit" value='${msg("doLogIn")}'/>
                    </div>
                </div>
            </div>
        </form>
    </#if>
</@layout.registrationLayout>
```

## Exercise 5: Authentication SPI


1. Create a new Authenticator
2. Configure Keycloak correctly
3. Test it http://localhost:8080/auth/realms/authentication/account