# Theming

## Which parts can be customised

* Account
* Admin
* Email
* Login
* Welcome

## Components

* HTML templates (Freemarker Templates)
* Images
* Message bundles
* Stylesheets
* Scripts (JavaScript)
* Theme properties

https://github.com/keycloak/keycloak/tree/master/themes/src/main/resources/theme

## Packaging and Deployment

* Folder located in the `/opt/jboss/keycloak/themes/` directory.
* Subfolder for each type (account, admin, email, login, welcome)
* File with the name `theme.properites` located in the type folder.

## Theme properties

### Most important properties

* parent - Parent theme to extend
* import - Import resources from another theme `<type>/<theme>` common/keycloak login/keycloak
* styles - Space-separated list of styles to include
* locales - Comma-separated list of supported locales

### Example
```properties
parent=keycloak
styles=node_modules/patternfly/dist/css/patternfly.css ...
```

## Exercise 4: Theming

1. Create a new custom theme
2. Configure Keycloak correctly
3. Test it http://localhost:8080/auth/realms/theming/account