# Secure a Quarkus Application

## Create Application

```
mvn io.quarkus:quarkus-maven-plugin:0.21.2:create \
    -DprojectGroupId=org.acme \
    -DprojectArtifactId=quarkus \
    -DclassName="org.acme.quickstart.GreetingResource" \
    -Dpath="/hello" \
    -Dextensions="keycloak"
```

`./mvnw compile quarkus:dev -Dquarkus.http.port=8081`

## Next Steps
1. Create a new client 
2. Add `keycloak.json` to `src/main/resources`
3. Add `@javax.annotation.security.RolesAllowed("ldap-user")` to the hello method

## Test
```
curl -X POST \
-d "username=bwilson&password=password&client_id=quarkus&grant_type=password" \
http://localhost:8080/auth/realms/demo/protocol/openid-connect/token \
| jq -r .access_token \
| read ACCESS_TOKEN
```

`curl -H "Authorization:Bearer $ACCESS_TOKEN" http://localhost:8081/hello`

## Exercise: Secure Application

Creat an application and secure it with Keycloak.

https://www.keycloak.org/docs/latest/securing_apps/index.html#openid-connect