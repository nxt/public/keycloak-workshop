# Keycloak Gatekeeper

## What is Keycloak Gatekeeper

Keycloak Gatekeeper is a proxy service that integrates with the Keycloak authentication service. 
The service supports both access tokens in browser cookie or bearer tokens.

## Documentation

### Official Documentation
https://www.keycloak.org/docs/latest/securing_apps/index.html#_keycloak_generic_adapte

### CLI Help

\scriptsize

```
docker run keycloak/keycloak-gatekeeper --help

NAME:
   keycloak-gatekeeper - is a proxy using the keycloak service for auth and authorization
USAGE:
   keycloak-gatekeeper [options]
VERSION:
   6.0.1 (git+sha: cd7ed04, built: 25-04-2019)
AUTHOR:
   Keycloak <keycloak-user@lists.jboss.org>
COMMANDS:
     help, h  Shows a list of commands or help for one command
GLOBAL OPTIONS:
   ...
```

\normalsize

## Exercise: Gatekeeper

1. Read https://www.keycloak.org/docs/latest/server_admin/index.html#_audience
2. Configure Gatekeeper in order that it works with the `echo` service.

Hint 1: https://www.keycloak.org/docs/latest/securing_apps/index.html#_keycloak_generic_adapter

Hint 2: `docker-compose restart gatekeeper`

Hint 3: Add the entry `127.0.0.1 keycloak` to your hosts file.
