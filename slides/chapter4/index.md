# Keycloak Setup

## Docker-Compose - Keycloak, LDAP and E-Mail

\scriptsize

```yaml
version: '3.4'
services:
  ldap:
    image: osixia/openldap:1.2.4
    environment:
      - LDAP_DOMAIN=keycloak.org
    ports:
      - "127.0.0.1:389:389"
    command: --copy-service
    volumes:
      - ./ldap:/container/service/slapd/assets/config/bootstrap/ldif/custom:ro
  mail:
    image: mailhog/mailhog
    ports:
      - "127.0.0.1:1025:1025"
      - "127.0.0.1:8025:8025"
  keycloak:
    build: keycloak
    ports:
      - "127.0.0.1:8080:8080"
```

\normalsize

## LDAP

* dc=keycloak,dc=org
  * cn=admin (password: admin)
  * ou=People
    * uid=bwilson (password: password)
    * uid=jbrown (password: password)
  * ou=FinanceRoles
    * cn=accountant (member: bwilson)
  * ou=RealmRoles
    * cn=ldap-admin (member: jbrown)
    * cn=ldap-user (member: bilson, jbrown)

## E-Mail

Web UI: [http://localhost:8025](http://localhost:8025/)

## Keycloak Dockerfile

```Dokerfile
FROM jboss/keycloak

ENV KEYCLOAK_USER admin
ENV KEYCLOAK_PASSWORD admin
```

## Exercise 1: Getting Started

1. Start Keycloak
2. Create a new Realm
3. Configure E-Mail settings
4. Configure LDAP settings
5. Add a ldap role mapper
6. Add a ldap group mapper
7. Test your setup (hint: http://localhost:8080/auth/realms/xxx/account)