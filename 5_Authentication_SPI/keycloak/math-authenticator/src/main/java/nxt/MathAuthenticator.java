package nxt;

import org.keycloak.authentication.AuthenticationFlowContext;
import org.keycloak.authentication.AuthenticationFlowError;
import org.keycloak.authentication.Authenticator;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.models.UserModel;

public class MathAuthenticator implements Authenticator {

    /**
     * Does this authenticator require that the user has already been identified?
     */
    public boolean requiresUser() {
        return false;
    }

    /**
     * Initial call for the authenticator.  This method should check the current HTTP request to determine if the request
     * satifies the Authenticator's requirements.  If it doesn't, it should send back a challenge response by calling
     * the AuthenticationFlowContext.challenge(Response).
     */
    public void authenticate(AuthenticationFlowContext context) {
        String equation = context.getAuthenticatorConfig().getConfig().get("equation");
        context.challenge(context.form().setAttribute("equation", equation).createForm("math.ftl"));
    }

    /**
     * Called from a form action invocation.
     */
    public void action(AuthenticationFlowContext context) {
        boolean validated = context.getHttpRequest().getDecodedFormParameters().getFirst("answer")
                .equals(context.getAuthenticatorConfig().getConfig().get("solution"));
        if (!validated) {
            context.failureChallenge(AuthenticationFlowError.INVALID_CREDENTIALS, context.form()
                    .setAttribute("equation", context.getAuthenticatorConfig().getConfig().get("equation"))
                    .setError("badResult")
                    .createForm("math.ftl"));
            return;
        }
        context.success();
    }

    /**
     * This method is responsible for determining if the user is configured for this particular authenticator.
     */
    public boolean configuredFor(KeycloakSession session, RealmModel realm, UserModel user) {
        return false;
    }

    /**
     * Set actions to configure authenticator.
     */
    public void setRequiredActions(KeycloakSession session, RealmModel realm, UserModel user) {

    }

    /**
     * This is called when the server shuts down.
     */
    public void close() {

    }
}
