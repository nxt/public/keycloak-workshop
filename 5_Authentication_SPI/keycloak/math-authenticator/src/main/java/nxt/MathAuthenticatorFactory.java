package nxt;

import org.keycloak.Config;
import org.keycloak.authentication.Authenticator;
import org.keycloak.authentication.AuthenticatorFactory;
import org.keycloak.models.AuthenticationExecutionModel;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;
import org.keycloak.provider.ProviderConfigProperty;

import java.util.ArrayList;
import java.util.List;

public class MathAuthenticatorFactory implements AuthenticatorFactory {

    private static final String PROVIDER_ID = "math-authenticator";
    private static final MathAuthenticator SINGLETON = new MathAuthenticator();

    public String getId() {
        return PROVIDER_ID;
    }

    public Authenticator create(KeycloakSession keycloakSession) {
        return SINGLETON;
    }

    /**
     * While there are four different requirement types: ALTERNATIVE, REQUIRED, OPTIONAL, DISABLED,
     * AuthenticatorFactory implementations can limit which requirement options are shown in the admin console when defining a flow.
     */
    public AuthenticationExecutionModel.Requirement[] getRequirementChoices() {
        return new AuthenticationExecutionModel.Requirement[]{
                AuthenticationExecutionModel.Requirement.REQUIRED,
                AuthenticationExecutionModel.Requirement.DISABLED
        };
    }

    /**
     * Text that will be shown in the admin console when listing the Authenticator.
     */
    public String getDisplayType() {
        return "Math Authenticator";
    }

    /**
     * Tooltip text that will be shown when you are picking the Authenticator you want to bind to an execution.
     */
    public String getHelpText() {
        return "Math Authenticator";
    }

    /**
     * Category the Authenticator belongs to.
     */
    public String getReferenceCategory() {
        return null;
    }

    /**
     * The AuthenticatorFactory.isUserSetupAllowed() is a flag that tells the flow manager whether or not Authenticator.setRequiredActions() method will be called.
     * If an Authenticator is not configured for a user, the flow manager checks isUserSetupAllowed().
     * If it is false, then the flow aborts with an error.
     * If it returns true, then the flow manager will invoke Authenticator.setRequiredActions().
     */
    public boolean isUserSetupAllowed() {
        return false;
    }

    /**
     * Flag which specifies to the admin console on whether the Authenticator can be configured within a flow
     */
    public boolean isConfigurable() {
        return true;
    }

    /**
     * Configuration option which is shown in the admin console.
     */
    public List<ProviderConfigProperty> getConfigProperties() {
        List<ProviderConfigProperty> configProperties = new ArrayList<>();
        ProviderConfigProperty property = new ProviderConfigProperty();
        property.setName("equation");
        property.setLabel("Equation");
        property.setType(ProviderConfigProperty.STRING_TYPE);
        property.setHelpText("Equation");
        configProperties.add(property);

        property = new ProviderConfigProperty();
        property.setName("solution");
        property.setLabel("Solution");
        property.setType(ProviderConfigProperty.STRING_TYPE);
        property.setHelpText("Solution");
        configProperties.add(property);
        return configProperties;
    }

    /**
     * Only called once when the factory is first created.
     */
    public void init(Config.Scope scope) {
    }

    /**
     * Called after all provider factories have been initialized
     */
    public void postInit(KeycloakSessionFactory keycloakSessionFactory) {

    }

    /**
     * This is called when the server shuts down.
     */
    public void close() {
    }
}
